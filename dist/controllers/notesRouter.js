const express = require('express');
const router = new express.Router();

const {
  getUserNotes,
  addUserNotes,
  getUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
  deleteUserNoteById,
} = require('../services/notesService');
const {
  wrap,
} = require('../asyncWrappers/apiWrap');

router.get('/', wrap(async (req, res) => {
  const { userId } = req.user;
  const offset = req.query.offset;
  const limit = req.query.limit;

  const notesByUser = await getUserNotes(userId, offset, limit);

  res.json({
    offset: notesByUser.validatedOffset,
    limit: notesByUser.validatedLimit,
    count: notesByUser.count,
    notes: notesByUser.notes,
  });
}));
router.get('/:id', wrap(async (req, res) => {
  const { userId } = req.user;
  const { id } = req.params;
  const note = await getUserNoteById(id, userId);

  if (!note) {
    throw new Error('No note with such id found');
  }

  res.json({ note });
}));
router.put('/:id', wrap(async (req, res) => {
  const { userId } = req.user;
  const { id } = req.params;
  const data = req.body;
  await updateUserNoteById(id, userId, data);

  res.json({ message: 'Note was updated successfully' });
}));
router.patch('/:id', wrap(async (req, res) => {
  const { userId } = req.user;
  const { id } = req.params;
  await toggleCompletedForUserNoteById(id, userId);

  res.json({ message: 'Success' });
}));
router.delete('/:id', wrap(async (req, res) => {
  const { userId } = req.user;
  const { id } = req.params;
  await deleteUserNoteById(id, userId);

  res.json({ message: 'Note was deleted' });
}));

router.post('/', wrap(async (req, res) => {
  const { userId } = req.user;
  await addUserNotes(userId, req.body);

  res.json({ message: 'Note was created successfully' });
}));

module.exports = {
  notesRouter: router,
};
