const express = require('express');
const router = new express.Router();

const { createProfile, login } = require('../services/authService');
const { wrap } = require('../asyncWrappers/apiWrap');

router.post('/register', wrap(async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  await createProfile({ username, password });

  res.json({ message: 'Account creeated successfully' });
}));

router.post('/login', wrap(async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  const token = await login({ username, password });

  res.json({ message: 'Success', jwt_token: token });
}));

module.exports = {
  authRouter: router,
};
