const { Note } = require('../models/noteModel');

const getUserNotes = async (userId, offset, limit) => {
  const limitNumb = +limit;
  const offsetNumb = +offset;
  let validatedLimit;
  let validatedOffset;

  if (!limitNumb || Math.round(limitNumb) < 10) {
    validatedLimit = 10;
  } else if (Math.round(limitNumb) > 50) {
    validatedLimit = 50;
  } else {
    validatedLimit = Math.round(limitNumb);
  }

  !offsetNumb ? validatedOffset = 0 : validatedOffset = Math.round(offsetNumb);

  const notes = await Note.find({ assigned_to: userId })
      .skip(validatedOffset)
      .limit(validatedLimit);

  const count = await Note.count({ assigned_to: userId });


  return { validatedOffset, validatedLimit, count, notes };
};
const getUserNoteById = async (noteId, userId) => {
  const note = await Note.findOne({ _id: noteId, userId });
  return note;
};

const addUserNotes = async (userId, notePayload) => {
  const note = new Note({ ...notePayload, userId });
  console.log(note);
  await note.save();
};

const updateUserNoteById = async (noteId, userId, data) => {
  await Note.findOneAndUpdate({ _id: noteId, userId }, { $set: data });
};
const toggleCompletedForUserNoteById = async (noteId, userId) => {
  const note = await Note.findOne({ _id: noteId, userId });

  note.completed = !note.completed;

  await note.save();
};

const deleteUserNoteById = async (noteId, userId) => {
  await Note.findOneAndRemove({ _id: noteId, userId });
};

module.exports = {
  getUserNotes,
  getUserNoteById,
  addUserNotes,
  updateUserNoteById,
  deleteUserNoteById,
  toggleCompletedForUserNoteById,
};
