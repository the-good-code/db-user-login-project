const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const PORT = process.env.PORT || 8080;

const { notesRouter } = require('./controllers/notesRouter');
const { authRouter } = require('./controllers/authRouter');
const { userRouter } = require('./controllers/userRouter');
const { authMiddleware } = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));


app.use('/api/auth', authRouter);
app.use('/api/users', [authMiddleware], userRouter);
app.use('/api/notes', [authMiddleware], notesRouter);

app.use((req, res, next) => {
  res.status(404).json({ message: 'Not found' });
});

app.use((err, req, res, next) => {
  res.status(400).json({ message: err.message });
});

const startApp = async () => {
  try {
    await mongoose.connect('mongodb+srv://test-user:RrXht2hPSRyy8afp@cluster0.t7wih.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
      useNewUrlParser: true, useUnifiedTopology: true,
    });

    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server start: ${err.message}`);
  }
};
startApp();
