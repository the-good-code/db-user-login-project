const jwt = require('jsonwebtoken');

const authMiddleware = async (req, res, next) => {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({
      message: 'Please, provide "authorization header"',
    });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(401)
        .json({ message: 'Please, include token to requset' });
  }
  try {
    const tokenPayload = await jwt.verify(token, 'secret');
    req.user = {
      userId: tokenPayload._id,
      username: tokenPayload.username,
    };
    next();
  } catch (err) {
    res.status(401).json({ message: err.message });
  }
};

module.exports = {
  authMiddleware,
};
